$(document).ready(function() {
  var lineData = {
    labels: ["Oct 11", "Oct 12", "Oct 13", "Oct 14"],
    datasets: [
      {
        label: "Sales",
        backgroundColor: "transparent",
        borderColor: "#FF8585",
        borderWidth: "3",
        pointBorderColor: "#FF8585",
        pointBackgroundColor: "#FF8585",
        data: [0, 2, 2.5, 3]
      },
      {
        label: "Favors",
        backgroundColor: "transparent",
        borderColor: "#558BF3",
        borderWidth: "3",
        pointBorderColor: "#558BF3",
        pointBackgroundColor: "#558BF3",
        data: [1, 2.1, 1.9, 3.5]
      },
      {
        label: "Views",
        backgroundColor: "transparent",
        borderColor: "#FFBF43",
        borderWidth: "3",
        pointBorderColor: "#FFBF43",
        pointBackgroundColor: "#FFBF43",
        data: [3, 6, 4.5, 15]
      }
    ]
  };

  var lineOptions = {
    responsive: false,
    elements: {
      line: {
        tension: 0
      }
    },
    scales: {
      yAxes: [
        {
          gridLines: {
            drawBorder: false
          },
          ticks: {
            suggestedMin: 0,
            suggestedMax: 20,
            stepSize: 5,
            padding: 20,
            callback: function(value, index, values) {
              return value + "k";
            },
            fontFamily: "Lato",
            fontSize: 14,
            fontColor: "#C3C1C7"
          }
        }
      ],
      xAxes: [
        {
          ticks: {
            beginAtZero: false,
            padding: 16,
            fontFamily: "Lato",
            fontSize: 14,
            fontColor: "#C3C1C7"
          },
          gridLines: {
            display: false,
            drawBorder: false
          }
        }
      ]
    },
    legend: {
      display: true,
      position: "bottom",
      labels: {
        usePointStyle: true,
        padding: 30
      }
    }
  };

  var ctx = document.getElementById("lineChart").getContext("2d");
  new Chart(ctx, { type: "line", data: lineData, options: lineOptions });
});
