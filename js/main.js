$(document).ready(function() {
  $(function() {
    setTimeout(function() {
      $(".user__container span").animate({ width: "toggle" });
    }, 2000);
  });

  $(".filter__choice").on("click", function() {
    if (!$(this).hasClass("active")) $(this).addClass("active");
    else $(this).removeClass("active");
  });

  $(".filter__form").on("click", function() {
    var flag = $(this)
      .parent()
      .hasClass("active");
    $(".filter__item")
      .children()
      .removeClass("active");
    $(".filter__item")
      .children()
      .find(".filter__dropdown")
      .slideUp();

    if (!flag) {
      $(this)
        .parent()
        .addClass("active");
      $(this)
        .parent()
        .find(".filter__dropdown")
        .slideDown();
    }
  });
  $('.modal__signup').modal({show: 'true'});
});
